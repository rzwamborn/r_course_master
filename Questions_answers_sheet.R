##########
#INTRO
##########

// QUESTION:

#1 create a vector named "myfirstvector" with a sequence of 5 values. (please don't make it an evently sequenctial sequence like 3,4,5 ect)
myfirstvector <- c(1,3,2,4,5)

##1.1 now access the 3rd element
myfirstvector[3]

##1.2order the values 
order(myfirstvector)

##1.3 assigned the sorted values to "myfirstvector"
myfirstvector <- sort(myfirstvector)

##1.4lastly remove the first value of the vector. 
myfirstvector[-1]

##########
#PART ONE
##########

// QUESTION:

#2 create a matrix named "myfirstmatrix" consisting of 4 columns and 3 rows 
myfirstmatrix <- matrix(data=1:16, nrow=4)
myfirstmatrix <- matrix(ncol=4, nrow=4)

#3 now create a data.frame of the same structure named "myfirstdataframe"
#tip creating a dataframe by defining each column might take some time...maybe you can combine what you have learned?
#hint:
#myfirstdataframe <- data.frame(matrix(..))
myfirstdataframe <- data.frame(matrix(ncol=4, nrow=5))

#3.1 check the number of columns and rows 
##3.2 assign the colnames of the previously defind df to the colnames of your new matrix 
colnames(myfirstdataframe) <- colnames(df)

##3.3 add a new column 
myfirstdataframe$newcolumn <- ""

##3.4. assign the value 10 to the first row of the first column of your matrix
myfirstmatrix[1,1] <- 10


##########
#PART TWO
##########

// QUESTION:

##4 we will work with de cars dataset in baseR:
#4.1 how many columns does the cars dataset have?
2

#4.2 and how many rows?
50

#4.3 how many cars had a stopping distance of 18ft in the cars dataset (column "dist")
table(cars$dist)

# 5 load your own dataset in the data and examine this within R (if you cannot find a dataset, please download one in surfdrive link) 
#there are no specific questions here, just play with some commands a little! 


##########
#PART THREE
##########

// QUESTION:

##6 Create your own plot of the animals or cars data. Try to make the kind of plot that is most logical to represent the data. Add as many features as possible like x and y labels, a title, legend, extra lines and colors. 
## Use the ?plot, ?hist, ?pie and ?barplot commands in your console to read the help file on the features of the type of plot you are using.   


##########
#PART FOUR
##########


// QUESTION:
# By Szymon M. Kiełbasa LUMC  

## 7. Create a random logical vector: 
v <- sample( c( T, F ), 10, replace = TRUE )
## Use which() to find positions with TRUE values. 
which( v == TRUE)

## 8. Create vector:
v <- c( "1", "B", "I", "O", "b" )
## check whether the elements of v are among LETTERS 
v %in% LETTERS
## check how many elements of v occur in letters
sum( v %in% LETTERS )

 // TIPS:
 #check out:
 #cheatsheets for usefull overviews, for example:
 ## https://www.rstudio.com/wp-content/uploads/2016/05/base-r.pdf
 #stackoverflow for questions
 #(advanced courses free on https://www.coursera.org/learn/r-programming
 
 
 ##BONUS questions: 
 #how would generate a random vector of 50 observations? 
 v <- sample(rep(LETTERS, length.out =50))
 
 #add this vector as an extra column to the cars data set. 
 cars$v <- v
 
 #what was the speed of the car with the higest stopping distance: 
cars[which.max(cars$dist),]